##### This is for test purpose #####


## Provisiong 1 VPC with terraform to AWS ##

Creating:

1. 1 VPC
2. 1 subnet public
3. 1 subnet private connected to 1 NAT Gateway
4. 1 autoscaling group

## Setup CI/CD environment with Gitlab-CI ##

- Create pipeline triggered by Gitlab webhook
- Deploy the apps with the docker from Dockerfile
- Push the docker image into AWS EC2 instance

