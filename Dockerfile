FROM nginx:1.19.8

RUN apt-get update && apt-get upgrade -y
RUN apt-get install –y nginx 

COPY myconf.conf /etc/nginx/conf.d/myconf.conf
COPY nginx.conf /etc/nginx/nginx.conf
COPY hello.txt /var/www/

EXPOSE 80

CMD [“echo”,”Image created”] 